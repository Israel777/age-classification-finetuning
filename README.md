**Age prediction fine tuning**


[the_source_blog](https://deeplearningsandbox.com/how-to-use-transfer-learning-and-fine-tuning-in-keras-and-tensorflow-to-build-an-image-recognition-94b0b02444f2)

**to train**  = python fine-tune.py --train_dir "data/images/train" --val_dir "data/images/test"

**to test with certain pics** python predict.py --model "model/2_vggface-fc.h5" --image "test/img.jpg"

**to test with web-cam** python cam.py
